package com.inutero.relaxingpomodoro

import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import java.util.concurrent.TimeUnit

class TimerAdapter(
    private val valueList: ArrayList<Pair<Long, String>>,
    private val viewPager2: ViewPager2
) :
    RecyclerView.Adapter<TimerAdapter.BackgroundViewHolder>() {

    private val runnable = Runnable {
        valueList.addAll(valueList)
        notifyDataSetChanged()
    }

    class BackgroundViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTimeValue: AppCompatTextView = itemView.findViewById(R.id.tvTimeValue)
        val tvTimeName: AppCompatTextView = itemView.findViewById(R.id.tvTimeName)
        var countDownTimer: CountDownTimer? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackgroundViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_time, parent, false)
        return BackgroundViewHolder(view)
    }

    override fun onBindViewHolder(holder: BackgroundViewHolder, position: Int) {
        if (holder.countDownTimer != null) {
            holder.countDownTimer?.cancel()
        }
        holder.countDownTimer = object : CountDownTimer(valueList[position].first, COUNT_DOWN_BY_SEC) {
            override fun onTick(millisUntilFinished: Long) {
                holder.tvTimeValue.text = (String.format("%02d : %02d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))))
            }

            override fun onFinish() {
                holder.tvTimeValue.text = "Finish"
            }
        }.start()

        holder.tvTimeName.text = valueList[position].second
        if (position == valueList.size - 1) {
            viewPager2.post(runnable)
        }
    }

//    override fun onBindViewHolder(holder: BackgroundViewHolder, position: Int) {
//        object : CountDownTimer(valueList[position].first, COUNT_DOWN_BY_SEC) {
//            override fun onTick(millisUntilFinished: Long) {
//                holder.tvTimeValue.text = (String.format("%02d : %02d",
//                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))))
//            }
//
//            override fun onFinish() {
//                holder.tvTimeValue.text = "Finish"
//            }
//        }.start()
//
//        holder.tvTimeName.text = valueList[position].second
//        if (position == valueList.size - 1) {
//            viewPager2.post(runnable)
//        }
//    }

    override fun getItemCount() = valueList.size

    companion object {
        private const val COUNT_DOWN_BY_SEC = 1000L
    }

}