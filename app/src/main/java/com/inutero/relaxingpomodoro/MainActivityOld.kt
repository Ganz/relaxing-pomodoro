package com.inutero.relaxingpomodoro

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2

class MainActivityOld : AppCompatActivity() {

    private lateinit var viewPager2Background: ViewPager2
    private lateinit var viewPager2Timer: ViewPager2
    private lateinit var handler: Handler
    private lateinit var imageList: ArrayList<Int>
    private lateinit var adapterBackground: BackgroundAdapter
    private lateinit var adapterTimer: TimerAdapter
    private var mediaPlayer: MediaPlayer? = null
    private val runnable = Runnable {
        viewPager2Timer.currentItem = viewPager2Timer.currentItem + 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        viewPager2Background.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                play(getSoundByPosition(backgroundPosition = position))
                handler.removeCallbacks(runnable)
                handler.postDelayed(runnable , SEC_5)
            }
        })
    }

    private fun init() {
        viewPager2Background = findViewById(R.id.viewPager2Background)
        viewPager2Timer = findViewById(R.id.viewPager2Timer)
        handler = Handler(Looper.myLooper()!!)
        imageList = ArrayList()

        setBackgroundList()
        setUpTransformer()

        adapterBackground = BackgroundAdapter(imageList, viewPager2Background, baseContext)
        adapterTimer = TimerAdapter(setTimerValueList(), viewPager2Timer)

        viewPager2Background.adapter = adapterBackground
        viewPager2Background.offscreenPageLimit = 1
        viewPager2Background.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER

        viewPager2Timer.adapter = adapterTimer
        viewPager2Timer.offscreenPageLimit = 3
        viewPager2Timer.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
    }

    private fun setUpTransformer(){
        val transformer = CompositePageTransformer()
        transformer.addTransformer(MarginPageTransformer(40))
        transformer.addTransformer { page, position ->
            val r = 1 - kotlin.math.abs(position)
            page.scaleY = 0.85f + r * 0.14f
        }

        viewPager2Timer.setPageTransformer(transformer)
    }

    private fun setBackgroundList() {
        imageList.add(R.raw.rain)
        imageList.add(R.raw.bonfire)
        imageList.add(R.raw.forest)
    }

    private fun setTimerValueList(): ArrayList<Pair<Long, String>> {
        val data = ArrayList<Pair<Long, String>>()
        data.add(Pair(SEC_90, "Work"))
        data.add(Pair(SEC_30, "Rest"))
        data.add(Pair(SEC_90, "Work"))
        return data
    }

    private fun play(soundId: Int) {
        stop()
        mediaPlayer = MediaPlayer.create(baseContext, soundId)
        mediaPlayer?.start()
    }

    private fun stop() {
        if (mediaPlayer != null) {
            mediaPlayer?.release()
            mediaPlayer = null
        }
    }

    private fun pauseSound() {
        if (mediaPlayer != null) {
            mediaPlayer?.pause()
        }
    }

    private fun resumeSound() {
        if (mediaPlayer != null) {
            mediaPlayer?.start()
        }
    }

    private fun getSoundByPosition(backgroundPosition: Int): Int {
        return when (backgroundPosition) {
            0 -> R.raw.rain_light
            1 -> R.raw.fire
            2 -> R.raw.forest_waterfall
            else -> R.raw.rain_light
        }
    }

    override fun onPause() {
        super.onPause()
        pauseSound()
        handler.removeCallbacks(runnable)
    }

    override fun onResume() {
        super.onResume()
        resumeSound()
        handler.postDelayed(runnable , SEC_2)
    }

    companion object {
        private const val SEC_2 = 2000L
        private const val SEC_5 = 5000L
        private const val SEC_30 = 30000L
        private const val SEC_90 = 90000L
    }

}