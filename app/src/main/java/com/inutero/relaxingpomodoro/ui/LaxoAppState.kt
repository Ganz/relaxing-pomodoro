package com.inutero.relaxingpomodoro.ui

sealed class Screen(val route: String) {
    object Home : Screen("home")
    object Player : Screen("player/{episodeUri}") {
        fun createRoute(episodeUri: String) = "player/${episodeUri}"
    }
}

class LaxoAppState(
    val navController: NavHostController
) {}